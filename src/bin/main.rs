use std::time::Instant;
use std::path::PathBuf;
use std::env;

use prc::analyze;

pub fn main() -> () {
    flexi_logger::Logger::try_with_env().unwrap().start().unwrap();
    let args: Vec<String> = env::args().collect();
    let file_path = PathBuf::from(args.get(1).unwrap());
    let timer = Instant::now();
    let result = analyze(&file_path);
    let dur = timer.elapsed();
    println!("Input {}: {} ({} ms)", file_path.as_os_str().to_str().unwrap(), result, dur.as_millis());
}
