use log::*;
use std::fs::File;
use std::io::{prelude::*, BufReader};
use std::path::Path;

pub fn analyze(path: &Path) -> usize {
    let problem = ProblemStatement::from_path(path);
    trace!("Solving {:?}", problem);
    problem.solve()
}

#[derive(Debug, Clone)]
struct ProblemStatement {
    seats_per_ride: usize,
    rides_per_day: usize,
    groups: Vec<usize>,
}

impl ProblemStatement {
    pub fn from_path(path: &Path) -> Self {
        let file = File::open(path).unwrap();
        let reader = BufReader::new(file);
        let mut lines = reader.lines();
        let header = lines.next().unwrap().unwrap();
        let mut header = header.split_ascii_whitespace();
        let seats_per_ride: usize = header.next().unwrap().parse().unwrap();
        let rides_per_day: usize = header.next().unwrap().parse().unwrap();
        let _capacity: usize = header.next().unwrap().parse().unwrap();
        let groups: Vec<usize> = lines
            .map(|group_size| group_size.unwrap().parse().unwrap())
            .collect();

        Self {
            seats_per_ride,
            rides_per_day,
            groups,
        }
    }

    /// Find the repetition pattern for the problem.
    ///
    /// Return Ok((rides, money)) if the pattern is found.
    ///
    /// Return Err(usize) with the money made if we reached
    /// the 'rides_per_day' limit before finding the pattern
    fn queue_pattern(&self, limit: usize) -> Result<(usize, usize), usize> {
        let mut cursor = self.groups.iter().cycle().enumerate().peekable();
        let mut rides_in_pattern = 0;
        let mut money_in_pattern = 0;
        loop {
            if rides_in_pattern % 5_000 == 0 {
                debug!(
                    "The pattern is currently {} rides long, hmmm",
                    rides_in_pattern
                );
            }

            // Early exit if we have too few rides
            if rides_in_pattern == limit {
                info!("Did not find pattern, returning found value");
                return Err(money_in_pattern);
            }

            // End-condition detection: the queue we see is exactly the same as the
            // initial one.
            if rides_in_pattern > 0 && cursor.peek().unwrap().0 % self.groups.len() == 0 {
                info!("Reached pattern recursion. Pattern is {} rides long", rides_in_pattern);
                return Ok((rides_in_pattern, money_in_pattern));
            }

            // Computing how much the ride makes this run, while advancing the cursor
            let mut ride_money = 0;
            let mut groups_in_ride = 0;
            let mut remaining_cap = self.seats_per_ride;
            // While there is enough room, and we still have groups waiting for the ride
            while cursor.peek().unwrap().1 <= &remaining_cap && groups_in_ride < self.groups.len() {
                let (_, group_size) = cursor.next().unwrap();
                ride_money = ride_money + group_size;
                remaining_cap = remaining_cap - group_size;
                groups_in_ride = groups_in_ride + 1;
            }

            trace!(
                "Ride {} makes {} people ({} groups)",
                rides_in_pattern + 1,
                ride_money,
                groups_in_ride
            );

            rides_in_pattern += 1;
            money_in_pattern += ride_money;
        }
    }

    pub fn solve(&self) -> usize {
        match self.queue_pattern(self.rides_per_day) {
            Err(amount) => amount,
            Ok(pattern) => {
                debug!("The pattern is {} rides long", pattern.0);
                let pattern_iterations = self.rides_per_day / pattern.0;
                let rem_after_patterns = self.rides_per_day % pattern.0;
                if rem_after_patterns > 0 {
                    pattern_iterations * pattern.1
                        + self.queue_pattern(rem_after_patterns).unwrap_err()
                } else {
                    pattern_iterations * pattern.1
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::path::PathBuf;

    #[test]
    fn simple_case() {
        let mut sample = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        sample.push("samples/1_simple_case.txt");
        assert_eq!(analyze(&sample), 7);
    }

    #[test]
    fn thousand_groups_of_few_people() {
        let mut sample = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        sample.push("samples/2_1000_groups_of_few_people.txt");
        assert_eq!(analyze(&sample), 3935);
    }

    #[test]
    fn the_same_groups_go_on_the_ride_several_times_during_the_day() {
        let mut sample = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        sample.push("samples/3_the_same_groups_go_on_the_ride_several_times_during_the_day.txt");
        assert_eq!(analyze(&sample), 15);
    }

    #[test]
    fn all_the_people_get_on_the_roller_coaster_at_least_once() {
        let mut sample = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        sample.push("samples/4_all_the_people_get_on_the_roller_coaster_at_least_once.txt");
        assert_eq!(analyze(&sample), 15000);
    }

    #[test]
    fn high_earnings_during_the_day() {
        let mut sample = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        sample.push("samples/5_high_earnings_during_the_day.txt");
        assert_eq!(analyze(&sample), 4999975000);
    }

    #[test]
    fn works_with_a_large_dataset() {
        let mut sample = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        sample.push("samples/6_works_with_a_large_dataset.txt");
        assert_eq!(analyze(&sample), 89744892565569);
    }

    #[test]
    fn hard() {
        let mut sample = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        sample.push("samples/7_hard.txt");
        assert_eq!(analyze(&sample), 8974489271113753);
    }

    #[test]
    fn harder() {
        let mut sample = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        sample.push("samples/8_harder.txt");
        assert_eq!(analyze(&sample), 89744892714152289);
    }
}
