# Notre petit secret à nous

```bash
RUST_LOG=trace cargo run --release -- samples/1_simple_case.txt
# Ou sinon
cargo test
```

Voir [les instructions](./INSTRUCTION.md)
